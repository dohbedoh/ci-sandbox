package com.dohbedoh.service;

import org.junit.Test;

/**
 * Tests for {@link SimpleOperations}.
 *
 * Created by Allan BURDAJEWICZ on 29/09/2015.
 */
public class SimpleOperationsTest {

    @Test
    public void testSimpleAddition() {
        SimpleOperations ops = new SimpleOperations();

        org.junit.Assert.assertEquals(1, ops.plus(1).getValue());
    }

    @Test
    public void testMultipleAdditions() {
        SimpleOperations ops = new SimpleOperations();

        org.junit.Assert.assertEquals(3, ops.plus(1).plus(2).getValue());
    }

    @Test
    public void testSimpleSubtraction() {
        SimpleOperations ops = new SimpleOperations();

        org.junit.Assert.assertEquals(-1, ops.minus(1).getValue());
    }

    @Test
    public void testMultipleSubtraction() {
        SimpleOperations ops = new SimpleOperations();

        org.junit.Assert.assertEquals(-3, ops.minus(1).minus(2).getValue());
    }

    @Test
    public void testSimpleDivision() {
        SimpleOperations ops = new SimpleOperations();

        org.junit.Assert.assertEquals(0, ops.divide(1).getValue());

        ops = new SimpleOperations(4);

        org.junit.Assert.assertEquals(2, ops.divide(2).getValue());
    }

    @Test(expected = ArithmeticException.class)
    public void testZeroDivision() {
        SimpleOperations ops = new SimpleOperations();

        org.junit.Assert.assertEquals(0, ops.divide(0).getValue());
    }
}

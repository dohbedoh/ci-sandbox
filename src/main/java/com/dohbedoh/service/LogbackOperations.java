package com.dohbedoh.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Service class: Simple operations for testing SLF4J and Logback.
 *
 * Created by Allan BURDAJEWICZ on 1/10/2015.
 */
public class LogbackOperations {

    private final Logger log = LoggerFactory.getLogger(LogbackOperations.class);

    public void trace(String message) {
        log.trace("message = {}", message);
    }

    public void debug(String message) {
        log.debug("message = {}", message);
    }

    public void info(String message) {
        log.info("message = {}", message);
    }

    public void warn(String message) {
        log.warn("message = {}", message);
    }

    public void error(String message) {
        log.error("message = {}", message);
    }
}

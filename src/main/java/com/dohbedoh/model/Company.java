package com.dohbedoh.model;

import java.util.Collection;

/**
 * Simple Company: A name, a list of employees.
 * <p>
 * Created by Allan on 1/10/2015.
 */
public class Company {

    private String companyName;
    private Collection<Employee> employees;

    public Company() {
    }

    public Company(String companyName, Collection<Employee> employees) {
        this.companyName = companyName;
        this.employees = employees;
    }

    public Collection<Employee> getEmployees() {
        return employees;
    }

    public String getCompanyName() {
        return companyName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Company company = (Company) o;

        return !(companyName != null ? !companyName.equals(company.companyName) : company.companyName != null);

    }

    @Override
    public int hashCode() {
        return companyName != null ? companyName.hashCode() : 0;
    }

    @Override
    public String toString() {
        return null;
    }
}
